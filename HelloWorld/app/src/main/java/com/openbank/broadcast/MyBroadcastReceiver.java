package com.openbank.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("*****************");
        Toast.makeText(context, "接收到的广播为："+intent.getAction(), Toast.LENGTH_LONG).show();
    }
}
