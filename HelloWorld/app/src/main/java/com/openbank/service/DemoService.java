package com.openbank.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.openbank.demo.R;

public class DemoService extends Service {

    /** 标识是否可以使用onRebind */
    boolean mAllowRebind;

    String sermsg="DemoService : ";

    /** 当服务被创建时调用. */
    @Override
    public void onCreate() {
        Log.d(sermsg,"onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int btnid=R.id.button2;
        if(btnid == startId){
            Toast.makeText(this, "btnid:"+btnid, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "btnid:"+btnid, Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "startId:"+startId, Toast.LENGTH_LONG).show();
        }
        Toast.makeText(this, "服务已经启动", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    /** 通过bindService()绑定到服务的客户端 */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(sermsg,"onBind");
        return null;
    }

    /** 通过unbindService()解除所有客户端绑定时调用 */
    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(sermsg,"onUnbind");
        return mAllowRebind;
    }

    /** 通过bindService()将客户端绑定到服务时调用*/
    @Override
    public void onRebind(Intent intent) {
        Log.d(sermsg,"onRebind");
    }

    /** 服务不再有用且将要被销毁时调用 */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this,"服务已经停止",Toast.LENGTH_LONG).show();
    }
}
