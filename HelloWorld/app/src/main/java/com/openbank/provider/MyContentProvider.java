package com.openbank.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class MyContentProvider extends ContentProvider {

    public static final String PROVIDER_NAME="com.openbank.provider.Valliage";
    public static final String URL="content://"+PROVIDER_NAME+"/persions";
    public static final Uri CONTENT_URI=Uri.parse(URL);

    public static final String ID="id";
    public static final String NAME="name";
    public static final String AGE="age";

    private static HashMap<String,String> PERSION_MAP;

    public static final int COUNTS=1;
    public static final int PERSION_ID=2;

    public static final UriMatcher uriMatcher;
    static {
        uriMatcher=new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME,"persions",COUNTS);
        uriMatcher.addURI(PROVIDER_NAME,"persions/#",PERSION_ID);
    }

    /**
     * 数据库特定常量声明
     */
    private SQLiteDatabase db;
    public static final String DATABASE_NAME="Valliage";
    public static final String PERSIONS_TABLE_NAME="persions";
    public static final int DATABASE_VERSION=1;
    public static final String CREATE_DB_TABLE="CREATE TABLE "+PERSIONS_TABLE_NAME+
            "(id INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "name TEXT NOT NULL,"+
            "age TEXT NOT NULL);";

    /**
     * 创建和管理提供者内部数据源的帮助类.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(@Nullable Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+PERSIONS_TABLE_NAME);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        Context context=getContext();
        DatabaseHelper databaseHelper=new DatabaseHelper(context);
        db=databaseHelper.getWritableDatabase();
        return (db==null)?false:true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(PERSIONS_TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case COUNTS:
                qb.setProjectionMap(PERSION_MAP);
                break;

            case PERSION_ID:
                qb.appendWhere( ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (sortOrder == null || sortOrder == ""){
            /**
             * 默认按照人员姓名排序
             */
            sortOrder = NAME;
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs,null, null, sortOrder);

        /**
         * 注册内容URI变化的监听器
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)){
            case COUNTS:
                return "vnd.android.cursor.dir/vnd.example.students";
            case PERSION_ID:
                return "vnd.android.cursor.item/vnd.example.students";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        /**
         * 添加新人员记录
         */
        long rowID = db.insert( PERSIONS_TABLE_NAME, "", values);

        /**
         * 如果记录添加成功
         */

        if (rowID > 0)
        {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)){
            case COUNTS:
                count = db.delete(PERSIONS_TABLE_NAME, selection, selectionArgs);
                break;

            case PERSION_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete( PERSIONS_TABLE_NAME, ID +  " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)){
            case COUNTS:
                count = db.update(PERSIONS_TABLE_NAME, values, selection, selectionArgs);
                break;

            case PERSION_ID:
                count = db.update(PERSIONS_TABLE_NAME, values, ID + " = " + uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +selection + ')' : ""), selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
