package com.openbank.demo;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.openbank.provider.MyContentProvider;
import com.openbank.service.DemoService;

public class MainActivity extends AppCompatActivity {

    String msg = "Android ： ";

    /** 当活动第一次被创建时调用 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");
        ImageButton ibtn=findViewById(R.id.imageButton);
        ibtn.setImageResource(R.drawable.maomi);
    }

    /** 当活动即将可见时调用 */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** 当活动可见时调用 */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "The onResume() event");
    }

    /** 当其他活动获得焦点时调用 */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** 当活动不再可见时调用 */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    /** 当活动将被销毁时调用 */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

    /**
     * 启动服务
     * @param view
     */
    public void startService(View view){
        startService(new Intent(getBaseContext(), DemoService.class));
    }

    /**
     * 停止服务
     * @param view
     */
    public void stopService(View view){
        stopService(new Intent(getBaseContext(),DemoService.class));
    }


    public void broadcastIntent(View view){
        Log.d(msg, "The broadcastIntent() event");
        Intent intent=new Intent();
        intent.setAction("com.openbank.broadcast.asdf");
        System.out.println("bbbbb");
        //第一个是发送广播的报名，第二个是接收广播的具体类
        intent.setComponent(new ComponentName("com.openbank.demo","com.openbank.broadcast.MyBroadcastReceiver"));
        System.out.println("aaaaaaa");
        sendBroadcast(intent,null);
    }

    public void addPersionName(View view){
        ContentValues values=new ContentValues();
        values.put(MyContentProvider.NAME,((EditText)findViewById(R.id.editText)).getText().toString());
        values.put(MyContentProvider.AGE, ((EditText)findViewById(R.id.editText2)).getText().toString());
        Uri uri = getContentResolver().insert(MyContentProvider.CONTENT_URI, values);
        Toast.makeText(getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();
    }

    public void rtrievePersions(View view) {
        // Retrieve student records
        String URL = "content://com.openbank.provider.Valliage/persions";

        Uri students = Uri.parse(URL);
        Cursor c = managedQuery(students, null, null, null, "name");

        if (c.moveToFirst()) {
            do{
                Toast.makeText(this,
                        c.getString(c.getColumnIndex(MyContentProvider.ID)) +
                                ", " +  c.getString(c.getColumnIndex( MyContentProvider.NAME)) +
                                ", " + c.getString(c.getColumnIndex( MyContentProvider.AGE)),
                        Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
    }
}
